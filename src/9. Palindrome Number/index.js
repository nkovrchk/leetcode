/**
 * @param {number} x
 * @return {boolean}
 */
let isPalindrome = function(x) {
    const str = String(x);
    let flag = true;

    for (let i = 0; i < str.length / 2; i++){
        const left = str[i];
        const right = str[str.length - 1 - i];

        if(left !== right){
            flag = false;
            break;
        }
    }

    return flag;
};

console.log(isPalindrome(121))

console.log(isPalindrome(-121))

console.log(isPalindrome(10))